﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Packets
{
    public class Packets
    {
        public const int Version = 1;

        public enum ToClient : byte
        {
            //Ping,
            //Pong,
            Disconnect,         ///[byte:код_команды][byte:причина_дисконекта]
            ErrorOperation,     //[byte:код_команды][byte:код_команды_которая_вызвала_ошибку][byte:код_ошибки]
            AcceptOperation,    //[byte:код_команды][byte:код_команды_которую_нужно_подтвердить]
            SendId,             //[byte:код_команды][int32:ID_игрока]
            SendMessage,        //[byte:код_команды][int32:адресат][int32:количество_символов][utf8:сообщение ...]
            PlayerConnect,      //[byte:код_команды][int32:ID_игрока]
            PlayerDiscinnect,   //[byte:код_команды][int32:ID_игрока]
            PlayerSetSnake,     //[byte:код_команды][int32:ID_игрока][byte:цвет1_R][byte:цвет1_G][byte:цвет1_B]
            SnakePosition,      //[byte:код_команды][int32:ID_игрока][int32:позиция_X][int32:позиция_Y]
        }

        public enum ToServer : byte
        {
            //Ping,
            //Pong,
            Disconnect,         //[byte:код_команды][byte:причина_дисконекта]
            CheckVersion,       //[byte:код_команды][int:версия]
            Authentication,     //[byte:код_команды][byte:назначение][byte[16]:хэш]
            SendMessage,        //[byte:код_команды][int32:адресат][int32:количество_символов][utf8:сообщение ...]
            SetSnake,           //[byte:код_команды][byte:цвет1_R][byte:цвет1_G][byte:цвет1_B]
            StartGame,          //[byte:код_команды]
            SetDirection        //[byte:код_команды][byte:направление]
        }
    }
}
