﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WorldOfSnakeServer.Code
{
    public enum Directions
    {
        Right, Down, Left, Up
    }

    public class Game
    {
        public Random Random { get; }

        public Point FildSize { get; }
        public List<Snake> Snakes { get; }
        public List<Food> Foods { get; }

        Task TaskWaitConnection;
        int IdCounter;

        public Game()
        {
            Random = new Random();
            Snakes = new List<Snake>();
            FildSize = new Point(10, 10);
            TaskWaitConnection = WaitConnectionAsync();
        }


        public void Update()
        {
            // Прочитать ввод
            for (int i = 0; i < Snakes.Count; i++)
                Snakes[i].Input();

            // Обновить состояние
            for (int i = 0; i < Snakes.Count; i++)
                Snakes[i].Update();

            // Расчитать коллизии
            // ...

            // Отправить состояние
            for (int i = 0; i < Snakes.Count; i++)
                Snakes[i].Output();


            Thread.Sleep(1000);
        }

        /// <summary>
        /// Асинхронно принимает входящие подключения
        /// </summary>
        async Task WaitConnectionAsync()
        {
            await Task.Run(() =>
            {
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listener.ReceiveBufferSize = 1024;
                listener.SendBufferSize = 1024;
                listener.Bind(new IPEndPoint(IPAddress.Any, 4455));
                listener.Listen(10);

                // цикл подключения
                while (true)
                {
                    Console.WriteLine("Ожидание подключения");
                    Socket client = listener.Accept();
                    client.ReceiveBufferSize = 1024;
                    client.SendBufferSize = 1024;
                    NetworkSnake snake = new NetworkSnake(this, client, IdCounter++);
                    Snakes.Add(snake);
                }
            });
        }
    }
}