﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Pack = Packets.Packets;

namespace WorldOfSnakeServer.Code
{
    public class NetworkSnake : Snake
    {
        public enum PlayerStates
        {
            WaitCheckVersion,
            WaitAuthentication,
            WaitSetSnake,
            WaitStartGame,
            Plaing
        }

        public int Id { get; }
        public Socket Client { get; }
        public PlayerStates PlayerState { get; private set; }

        byte[] receiveBufferTemp; // буфер приёма
        Directions nextDirection = Directions.Right;

        public NetworkSnake(Game game, Socket client, int id) : base(game)
        {
            Client = client;
            Id = id;
            receiveBufferTemp = new byte[Client.ReceiveBufferSize];

            ReceiveLoopAsync();

            Console.WriteLine($"Размер буфера: {Client.ReceiveBufferSize}");
        }

        void ReceiveLoopAsync()
        { Task.Run(() => { while (true) Receive(); }); }

        void Receive()
        {
            // Получаем сообщение
            int countReceived = Client.Receive(receiveBufferTemp);
            // Обрабатываем
            if (countReceived > 0)
            {
                Pack.ToServer command = (Pack.ToServer)receiveBufferTemp[0];

                if (command == Pack.ToServer.CheckVersion)
                {
                    Console.WriteLine($"{Id}:Попытка подключения");
                    int version = BitConverter.ToInt32(receiveBufferTemp, 1);

                    if (version == Pack.Version) // Выдать идентификатор клиенту
                    {
                        Task.Run(() => SendId(Id));
                        Console.WriteLine($"{Id}:Клиент успешно подключён");
                    }
                    else // Отправить сообщение об ошибке
                    {
                        Task.Run(() => SendError(0));
                        Console.WriteLine($"{Id}:Ошибка подключения");
                    }
                }
                else if (command == 1) // начать игру
                {
                    State = States.Play;

                    Console.WriteLine($"{Id}:Начинает игру");
                }
                else if (command == 128) // изменить направление движения змеи
                {
                    byte direction = receiveBufferTemp[1];
                    if (direction >= 0 && direction <= 4)
                        nextDirection = (Directions)direction;

                    Console.WriteLine($"{Id}:Изменить направление: {direction}");
                }
                else // неизвестная команда
                {
                    Console.WriteLine($"{Id}:Неизвестная команда: {command}");
                }
            }
        }

        void SendError(int error)
        {
            byte[] data = new byte[1 + 4];
            data[0] = 0;

            byte[] bytes = BitConverter.GetBytes(error);
            for (int i = 0; i < 4; i++)
                data[i + 1] = bytes[i];

            Client.Send(data);
        }
        void SendId(int id)
        {
            byte[] data = new byte[1 + 4];
            data[0] = 1;

            byte[] bytes = BitConverter.GetBytes(id);
            for (int i = 0; i < 4; i++)
                data[i + 1] = bytes[i];

            Client.Send(data);
        }
        void SendSnakeState(int id, int x, int y, int speed)
        {
            byte[] data = new byte[17];
            data[0] = 128;

            byte[] bytes = BitConverter.GetBytes(id);
            for (int i = 0; i < 4; i++)
                data[i + 1] = bytes[i];

            bytes = BitConverter.GetBytes(x);
            for (int i = 0; i < 4; i++)
                data[i + 5] = bytes[i];

            bytes = BitConverter.GetBytes(y);
            for (int i = 0; i < 4; i++)
                data[i + 9] = bytes[i];

            bytes = BitConverter.GetBytes(speed);
            for (int i = 0; i < 4; i++)
                data[i + 13] = bytes[i];

            Client.Send(data);
            //Console.WriteLine("Данные отправлены");
        }


        public override void Input()
        {
            if (State == States.Play)
            {
                // изменение направления движения змеи
                if (Direction != nextDirection)
                {
                    switch (nextDirection)
                    {
                        case Directions.Right:
                            if (Direction != Directions.Left)
                                Direction = nextDirection;
                            else
                                nextDirection = Direction;
                            break;
                        case Directions.Down:
                            if (Direction != Directions.Up)
                                Direction = nextDirection;
                            else
                                nextDirection = Direction;
                            break;
                        case Directions.Left:
                            if (Direction != Directions.Right)
                                Direction = nextDirection;
                            else
                                nextDirection = Direction;
                            break;
                        case Directions.Up:
                            if (Direction != Directions.Down)
                                Direction = nextDirection;
                            else
                                nextDirection = Direction;
                            break;
                    }
                }
            }
        }
        public override void Output()
        {
            if (State == States.Play)
            {
                Task.Run(() => SendSnakeState(Id, Segments[0].X, Segments[0].Y, (int)Speed));
            }
        }
    }
}
