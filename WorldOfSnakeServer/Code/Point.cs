﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldOfSnakeServer.Code
{
    public struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Point operator +(Point obj1, Point obj2)
        {
            return new Point(obj1.X + obj2.X, obj1.Y + obj2.Y);
        }
        public static Point operator -(Point obj1, Point obj2)
        {
            return new Point(obj1.X - obj2.X, obj1.Y - obj2.Y);
        }

        public static Point operator *(Point obj1, Point obj2)
        {
            return new Point(obj1.X * obj2.X, obj1.Y * obj2.Y);
        }
        public static Point operator /(Point obj1, Point obj2)
        {
            return new Point(obj1.X / obj2.X, obj1.Y / obj2.Y);
        }

        public static Point operator *(Point obj1, int obj2)
        {
            return new Point(obj1.X * obj2, obj1.Y * obj2);
        }
        public static Point operator /(Point obj1, int obj2)
        {
            return new Point(obj1.X / obj2, obj1.Y / obj2);
        }

        public static bool operator ==(Point obj1, Point obj2)
        {
            return obj1.X == obj2.X && obj1.Y == obj2.Y ;
        }
        public static bool operator !=(Point obj1, Point obj2)
        {
            return obj1.X != obj2.X || obj1.Y != obj2.Y;
        }
    }
}
