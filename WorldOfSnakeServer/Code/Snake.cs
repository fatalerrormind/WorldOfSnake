﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldOfSnakeServer.Code
{
    public class Snake
    {
        public enum States
        {
            Wait,
            Play
        }

        // Экземпляр игры
        public Game Game { get; }

        // Состояние змеи
        public States State { get; protected set; }

        // Координаты всех сегментов, [0] - это голова
        public List<Point> Segments { get; }

        // Направление движения
        public Directions Direction { get; set; }

        // Скорость в ячейках в секунду
        public float Speed { get; private set; }

        public Snake(Game game)
        {
            Game = game;
            State = States.Wait;
            Segments = new List<Point>();
            Point pos = new Point(Game.Random.Next(1, Game.FildSize.X - 1), Game.Random.Next(1, Game.FildSize.Y - 1));
            Segments.Add(pos);
            pos.X--;
            Segments.Add(pos);
            Direction = Directions.Right;
            Speed = 2f;
        }

        public void Update()
        {
            if (State == States.Play)
            {
                for (int i = Segments.Count - 1; i < 1; i--)
                    Segments[i] = Segments[i - 1];

                if (Direction == Directions.Right)
                    Segments[0] += new Point(1, 0);
                else if (Direction == Directions.Down)
                    Segments[0] += new Point(0, 1);
                else if (Direction == Directions.Left)
                    Segments[0] += new Point(-1, 0);
                else if (Direction == Directions.Up)
                    Segments[0] += new Point(0, -1);
            }
        }

        public virtual void Input() { }
        public virtual void Output() { }
    }
}
