﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MoonEngine;
using MoonEngine.Components;
using MoonEngine.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WorldOfSnake.Code.Forms
{
    public class MainMenu : Form
    {
        Label label;
        Button btnCreateGame, btnConnect, btnExit;
        Button btnSetDirRight, btnSetDirDown, btnSetDirLeft, btnSetDirUp;
        StackTemplate st;
        Socket server;
        byte[] receiveBufferTemp; // буфер приёма

        public MainMenu(Core core) : base(core)
        {
            ClearColor = Color.CornflowerBlue;

            label = new Label(Core, "State\n");
            label.Margin = new Margin(2);
            Bind(label);

            btnCreateGame = new Button(Core, "Create Game");
            btnCreateGame.Size = new Point(100, 20);
            btnCreateGame.Margin = new Margin(2);
            btnCreateGame.IsVisualReactionIsRightDown = false;
            Bind(btnCreateGame);

            btnConnect = new Button(Core, "Connect");
            btnConnect.Size = new Point(100, 20);
            btnConnect.Margin = new Margin(2);
            btnConnect.IsVisualReactionIsRightDown = false;
            Bind(btnConnect);

            btnExit = new Button(Core, "Exit");
            btnExit.Size = new Point(100, 20);
            btnExit.Margin = new Margin(2);
            btnExit.IsVisualReactionIsRightDown = false;
            Bind(btnExit);


            btnSetDirRight = new Button(Core, "R");
            btnSetDirRight.Size = new Point(20, 20);
            btnSetDirRight.Margin = new Margin(2);
            btnSetDirRight.IsVisualReactionIsRightDown = false;
            Bind(btnSetDirRight);

            btnSetDirDown = new Button(Core, "D");
            btnSetDirDown.Size = new Point(20, 20);
            btnSetDirDown.Margin = new Margin(2);
            btnSetDirDown.IsVisualReactionIsRightDown = false;
            Bind(btnSetDirDown);

            btnSetDirLeft = new Button(Core, "L");
            btnSetDirLeft.Size = new Point(20, 20);
            btnSetDirLeft.Margin = new Margin(2);
            btnSetDirLeft.IsVisualReactionIsRightDown = false;
            Bind(btnSetDirLeft);

            btnSetDirUp = new Button(Core, "U");
            btnSetDirUp.Size = new Point(20, 20);
            btnSetDirUp.Margin = new Margin(2);
            btnSetDirUp.IsVisualReactionIsRightDown = false;
            Bind(btnSetDirUp);

            st = new StackTemplate();
            st.Components.Add(label);
            st.Components.Add(btnCreateGame);
            st.Components.Add(btnConnect);
            st.Components.Add(btnExit);
            st.Components.Add(btnSetDirRight);
            st.Components.Add(btnSetDirDown);
            st.Components.Add(btnSetDirLeft);
            st.Components.Add(btnSetDirUp);
            st.HorizontalAlignment = HorizontalAlignment.Center;
            st.VerticalAlignment = VerticalAlignment.Center;
            st.Orientation = Orientation.Vertical;
        }
        override public void Update()
        {
            for (int i = 0; i < Components.Count; i++)
                Components[i].Update();

            if (btnCreateGame == Core.Control.LeftClicked)
            {
                Task.Run(() => SendStart());
            }
            else if (btnConnect == Core.Control.LeftClicked)
            {
                // Соединяемся с удаленным устройством
                server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                server.ReceiveBufferSize = 1024;
                server.SendBufferSize = 1024;
                receiveBufferTemp = new byte[server.ReceiveBufferSize];
                server.Connect("localhost", 4455);

                ReceiveLoopAsync();

                Task.Run(() => SendConnect(1));
            }
            else if (btnExit == Core.Control.LeftClicked)
            {
                // Освобождаем сокет
                server.Shutdown(SocketShutdown.Both);
                server.Close();

                Core.Game.Exit();
            }
            else if (btnSetDirRight == Core.Control.LeftClicked)
            {
                Task.Run(() => SendDirection(0));
            }
            else if (btnSetDirDown == Core.Control.LeftClicked)
            {
                Task.Run(() => SendDirection(1));
            }
            else if (btnSetDirLeft == Core.Control.LeftClicked)
            {
                Task.Run(() => SendDirection(2));
            }
            else if (btnSetDirUp == Core.Control.LeftClicked)
            {
                Task.Run(() => SendDirection(3));
            }
        }


        public override void Layout()
        {
            for (int i = 0; i < Components.Count; i++)
                Components[i].Layout();

            if (IsInvalidateLayout)
            {
                // Расчёт шаблона
                Size = Core.ScreenSize;
                Pos = Point.Zero;

                st.Pos = Pos;
                st.Size = Size;
                st.CalcPos();

                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }
        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Render();

                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
                // Отрисовка под UI
                // ...
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Draw();
                // Отрисовка над UI
                // ...
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }



        void ReceiveLoopAsync()
        { Task.Run(() => { while (true) Receive(); }); }

        void Receive()
        {
            // Получаем сообщение
            int countReceived = server.Receive(receiveBufferTemp);
            // Обрабатываем
            if (countReceived > 0)
            {
                byte command = receiveBufferTemp[0];

                if (command == 0) // отказ в подключении
                {
                    // Освобождаем сокет
                    server.Shutdown(SocketShutdown.Both);
                    server.Close();
                    label.Text = "В подключении отказано\n";
                }
                else if (command == 1) // принять id
                {
                    int id = BitConverter.ToInt32(receiveBufferTemp, 1);
                    label.Text = $"Id принят: {id}\n";
                }
                else if (command == 128) // изменить направление движения змеи
                {
                    int id = BitConverter.ToInt32(receiveBufferTemp, 1);
                    int x = BitConverter.ToInt32(receiveBufferTemp, 5);
                    int y = BitConverter.ToInt32(receiveBufferTemp, 9);
                    int speed = BitConverter.ToInt32(receiveBufferTemp, 13);

                    label.Text = $"id:{id} x:{x} y:{y} speed:{speed}\n";
                }
                else // неизвестная команда
                {
                    label.Text = $"Неизвестная команда: {command}\n";
                }
            }
            else
                label.Text = "Нет команды\n";
        }

        void SendConnect(int version)
        {
            byte[] data = new byte[5];
            data[0] = 0;

            byte[] bytes = BitConverter.GetBytes(version);
            for (int i = 0; i < 4; i++)
                data[i + 1] = bytes[i];

            server.Send(data);
        }

        void SendStart()
        {
            byte[] data = new byte[1];
            data[0] = 1;

            server.Send(data);
        }

        void SendDirection(byte direction)
        {
            byte[] data = new byte[2];
            data[0] = 128;
            data[1] = direction;
            server.Send(data);
        }
    }
}
