﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MoonEngine;
using MoonEngine.Test;
using System;
using System.Drawing;
using System.Windows.Forms;
using WorldOfSnake.Code.Forms;

namespace WorldOfSnake.Code
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MainGame : Game
    {
        Core core;
        GraphicsDeviceManager gdm;

        public MainGame()
        {
            gdm = new GraphicsDeviceManager(this);
            gdm.PreferredBackBufferWidth = 600;
            gdm.PreferredBackBufferHeight = 400;
            gdm.IsFullScreen = Settings.DefIsFullScreen;
            gdm.SynchronizeWithVerticalRetrace = Settings.DefSynchronizeWithVerticalRetrace;
            gdm.ApplyChanges();

            // 83333 ticks - 120 fps
            //TargetElapsedTime = new TimeSpan(83333);
            //TargetElapsedTime = new TimeSpan(1000000);
            //TargetElapsedTime = new TimeSpan(2000000);
            IsFixedTimeStep = false;
            IsMouseVisible = true;
            Window.IsBorderless = false;
            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += Window_ClientSizeChanged;

            // Замена системного курсора
            Bitmap cur = new Bitmap(@"Content\windows_cursor_normal.png");
            IntPtr ptr = cur.GetHicon();
            Cursor c = new Cursor(ptr);
            System.Windows.Forms.Form.FromHandle(this.Window.Handle).Cursor = c;

            Content.RootDirectory = "Content";
        }

        private void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            core.Forms.InvalidateLayout();
            core.Forms.InvalidateBuffer();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();

            core = new Core(this, gdm);
            core.Forms.Add(new Forms.MainMenu(core));
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (core.Input.IsKeyUpDown(Microsoft.Xna.Framework.Input.Keys.Escape))
                Exit();
            core.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            core.Draw(gameTime);
            base.Draw(gameTime);
        }
    }
}
